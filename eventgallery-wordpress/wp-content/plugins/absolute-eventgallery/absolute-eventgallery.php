<?php
/*
Plugin Name: Absolute Event Gallery
Plugin URI: http://www.absolute.ch
Description: Awesome Event Gallery Plugin
Author: Thomas Brandenburger
Version: 1.0
Author URI: http://www.absolute.ch
*/

if( ! defined('ABSPATH') ){
    exit();
}

add_filter( 'foogallery_gallery_posttype_register_args', 'adeg_foogallery_register' );
function adeg_foogallery_register() {
    //allow extensions to override the gallery post type
    $args = array(
            'labels'        => array(
                'name'               => __( 'Galleries', 'foogallery' ),
                'singular_name'      => __( 'Gallery', 'foogallery' ),
                'add_new'            => __( 'Add Gallery', 'foogallery' ),
                'add_new_item'       => __( 'Add New Gallery', 'foogallery' ),
                'edit_item'          => __( 'Edit Gallery', 'foogallery' ),
                'new_item'           => __( 'New Gallery', 'foogallery' ),
                'view_item'          => __( 'View Gallery', 'foogallery' ),
                'search_items'       => __( 'Search Galleries', 'foogallery' ),
                'not_found'          => __( 'No Galleries found', 'foogallery' ),
                'not_found_in_trash' => __( 'No Galleries found in Trash', 'foogallery' ),
                'menu_name'          => foogallery_plugin_name(),
                'all_items'          => __( 'Galleries', 'foogallery' )
            ),
            'hierarchical'  => false,
            'public'        => false,
            'rewrite'       => false,
            'show_ui'       => true,
            'show_in_menu'  => true,
            'menu_icon'     => 'dashicons-format-gallery',
            'supports'      => array( 'title', 'thumbnail', ),
            'show_in_rest' => true
    );
    return $args;
}


add_action( 'rest_api_init', 'adeg_apirest_register_foogallery_attachments' );
function adeg_apirest_register_foogallery_attachments() {
    register_rest_field( 'foogallery',
        'foogallery_attachments',
        array(
            'get_callback'    => 'adeg_get_foogallery_attachment',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}


function adeg_get_foogallery_attachment( $object, $field_name, $request ) {

    $key1_metaIds = get_post_meta( $object[ 'id' ], $field_name, true );
    $imageObjects = Array();

    foreach($key1_metaIds as $imagePostID)
    {
        $imagePost = get_post($imagePostID);

        $getAttachmentThumbnail = wp_get_attachment_image_src($imagePostID, $size = 'thumbnail');
        $getAttachmentFull = wp_get_attachment_image_src($imagePostID, $size = 'full');
        $thumbnail_src = $getAttachmentThumbnail[0];
        $full_src = $getAttachmentFull[0];

        $imagePostObject = Array(
            'title'         => $imagePost->post_title,
            'src'           => $imagePost->guid,
            'description'   => $imagePost->post_content,
            'thumbnail_src' => $thumbnail_src,
            'full_src' => $full_src,
        );
        array_push($imageObjects, $imagePostObject);
    }


    return $imageObjects;
}
