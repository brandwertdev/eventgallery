angular.module('starter.services', [])
    .factory('Albums', function ($http)
    {
        // Might use a resource here that returns a JSON array

        return {

            all: function ()
            {
                return $http.get('http://staging.absolute.ch/demo/eventgallery/wp-json/wp/v2/foogallery/')
                    .then(function (response)
                    {
                        var albums = [];
                        var responseData = response.data;

                        for (var i in responseData)
                        {

                            var newAlbumsObj = {
                                id: responseData[i].id,
                                name: responseData[i].title.rendered,
                                lastText: '',
                                face: responseData[i].foogallery_attachments[0].src
                            };

                            albums.push(newAlbumsObj);
                        }

                        return albums;

                    },
                    function (httpError)
                    {
                        console.log(httpError);
                    });
            }
        };
    })

    .factory('Photos', function ($http, $localStorage)
    {

        return {

            all: function ()
            {

                return $http({
                    method: 'GET',
                    cache: true,
                    url: 'http://staging.absolute.ch/demo/eventgallery/wp-json/wp/v2/foogallery/'
                    })
                    .then(function (response)
                    {
                        var images = [];
                        var responseData = response.data;

                        for (var i in responseData)
                        {
                            var thumbnailGalleryImages = responseData[i].foogallery_attachments;

                            console.log(thumbnailGalleryImages);

                            for(var ii in thumbnailGalleryImages ){
                                var newImgObj = {
                                    thumbnail_src: thumbnailGalleryImages[ii].thumbnail_src,
                                    full_src: thumbnailGalleryImages[ii].full_src
                                };

                                images.push(newImgObj);
                            }
                        }

                        $localStorage.images = images;
                        return images;
                    },
                    function (httpError)
                    {
                        //if no internet connection get json from cache
                        return $localStorage.images;

                    });

            },

            getAlbumImages: function (albumId)
            {
                var url = "http://staging.absolute.ch/demo/eventgallery/wp-json/wp/v2/foogallery/" + albumId;
                return $http.get(url)
                    .then(function (response)
                    {
                        var images = [];
                        var responseData = response.data;

                        for (var i in responseData.foogallery_attachments)
                        {

                            var newImgObj = {
                                src: responseData.foogallery_attachments[i].src,
                                sub: responseData.foogallery_attachments[i].title
                            };

                            images.push(newImgObj);
                        }
                        return images;
                    });

            }

        }
    });

