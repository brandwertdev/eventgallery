/**
 * Created by thomas on 29.02.16.
 */
angular.module('starter.directives', [])
    .directive('fadeIn', function($timeout){
    return {
        restrict: 'A',
        link: function($scope, $element, attrs){
            $element.addClass("ng-fadeout");
            $element.on('load', function() {
                $element.addClass("ng-fadein");
            });
        }
    }
})