angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($scope, $ionicModal, $timeout)
    {
        // Form data for the login modal
        $scope.loginData = {};

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function (modal)
        {
            $scope.modal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function ()
        {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function ()
        {
            $scope.modal.show();
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function ()
        {
            console.log('Doing login', $scope.loginData);

            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            $timeout(function ()
            {
                $scope.closeLogin();
            }, 1000);
        };
    })

    .controller('DashCtrl', function ($scope, $cordovaSocialSharing,  $ionicBackdrop, $ionicModal, $ionicSlideBoxDelegate, $ionicScrollDelegate, $ionicLoading, Photos)
    {

        var vm = this;

        vm.zoomMin = 1;

        vm.loadImages = function ()
        {

            $ionicLoading.show({
                template: 'Loading...'
            });

            Photos.all()
                .then(function (imageData)
                {
                    vm.slides = imageData;
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });

        }

        vm.showImages = function(index) {
            vm.activeSlide = index;
            vm.showModal('templates/gallery-zoomview.html');
        };

        vm.showModal = function(templateUrl) {
            $ionicModal.fromTemplateUrl(templateUrl, {
                scope: $scope
            }).then(function(modal) {
                vm.modal = modal;
                vm.modal.show();
            });
        }

        vm.closeModal = function() {
            console.log("close");
            vm.modal.hide();
            vm.modal.remove()
        };

        vm.zoom = function(slide){
            console.log("zoom");
            $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).zoomBy(2, true);
        };

        vm.updateSlideStatus = function(slide) {

            var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
            if (zoomFactor == vm.zoomMin) {
                $ionicSlideBoxDelegate.enableSlide(true);
            } else {
                $ionicSlideBoxDelegate.enableSlide(false);
            }
        };

        vm.mustBeVisible = function(index) {
            return Math.abs($ionicSlideBoxDelegate.currentIndex() - index) <= 2;
        }


        vm.shareImage = function(imgSrc){

            $cordovaSocialSharing.share('Message via EventApp',
                "Share vie EventApp",
                imgSrc,
                "http://www.absolute.ch")
        }

    })

    .controller('AlbumsCtrl', function ($scope, $ionicLoading, Albums)
    {
        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //
        //$scope.$on('$ionicView.enter', function(e) {
        //});
        var vm = this;

        vm.loadAlbums = function ()
        {

            $ionicLoading.show({
                template: 'Loading...'
            });

            Albums.all()
                .then(function (albumData)
                {
                    vm.albums = albumData;
                    $ionicLoading.hide();
                });
        }
    })

    .controller('AlbumDetailCtrl', function ($scope, $ionicLoading, $stateParams, Photos)
    {

        var vm = this;

        vm.items = [
            {
                src: 'http://staging.absolute.ch/demo/eventgallery/wp-content/uploads/2016/02/download-russian-wedding-pictures.jpg',
                sub: 'This is a <b>subtitle</b>'
            },
            {
                src: 'http://staging.absolute.ch/demo/eventgallery/wp-content/uploads/2016/02/468645-wedding-flowers.jpg',
                sub: '' /* Not showed */
            }
        ]

        vm.loadAlbumImages = function ()
        {

            $ionicLoading.show({
                template: 'Loading...'
            });

            Photos.getAlbumImages($stateParams.albumId).then(function (imageData)
            {
                vm.items = imageData;
                $ionicLoading.hide();
            });
        }

    })

    .controller('AccountCtrl', function ($scope)
    {
        $scope.settings = {
            enableFriends: true
        };
    });
