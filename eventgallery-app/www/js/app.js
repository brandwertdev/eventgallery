// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngStorage', 'ngCordova', 'starter.controllers', 'starter.services', 'starter.directives'])

    .run(function ($ionicPlatform)
    {
        $ionicPlatform.ready(function ($httpProvider, $cacheFactory)
        {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard)
            {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar)
            {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider)
    {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            // Each tab has its own nav history stack:

            .state('app.dash', {
                url: '/dash',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/app-dash.html',
                        controller: 'DashCtrl',
                        controllerAs: 'vm'
                    }
                }
            })

            .state('app.albums', {
                url: '/albums',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/app-albums.html',
                        controller: 'AlbumsCtrl',
                        controllerAs: 'vm'
                    }
                }
            })

            .state('app.album-detail', {
                url: '/albums/:albumId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/album-detail.html',
                        controller: 'AlbumDetailCtrl',
                        controllerAs: 'vm'
                    }
                }
            })

            .state('app.account', {
                url: '/account',
                views: {
                    'tab-account': {
                        templateUrl: 'templates/app-account.html',
                        controller: 'AccountCtrl'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/dash');

        $ionicConfigProvider.scrolling.jsScrolling(false);

    });
