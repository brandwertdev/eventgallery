# Eventgallery

Eine Eventgalerie Ionic / Cordova APP, die über ein REST API die Bilder/Daten aus einer Wordpress Instanz zieht.
Die APP wurde noch mit AngularJS 1.5 entwickelt und ist nun im Umbau auf AngularJS 2.0


## Funktionsauszug
* Erstellung und Verwaltung von beliebigen Fotoalben in Wordpress
* Multi-Images Upload
* Darstellung Bilder und Slideshow Funktion in APP
* Zoom/Swipe Funktion
* Social Media Sharing (Facebook, Twitter, Instagram)


## Technologien
* Ionic
* AngularJS 1.5
* Wordpress (php)
* WP REST API